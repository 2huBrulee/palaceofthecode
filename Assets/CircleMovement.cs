﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleMovement : MonoBehaviour {
    private float speed = 5;

	// Use this for initialization
	void Start () {
        Debug.Log("start");
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 playerInput = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"), 0).normalized;
        Vector3 speedVector = playerInput * speed * Time.deltaTime;
        //transform.position += speedVector;
        transform.Translate(speedVector);

    }

    void Awake() {
        Debug.Log("awake debug :D ");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Bullet")
        {
            Destroy(gameObject);
        }
    }
}
