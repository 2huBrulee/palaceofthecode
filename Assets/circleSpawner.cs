﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class circleSpawner : MonoBehaviour {
    public GameObject preFag;
    Camera cam;
    float height;
    float width;
    float cerraElTuje;

    // Use this for initialization
    void Start () {
        cerraElTuje = 0;
        cam = Camera.main;
        height = 2f * cam.orthographicSize;
        width = height * cam.aspect;
        Debug.Log("width: " + width);
        Debug.Log("height: " + height);
        
    }

    // Update is called once per frame
    void Update () {
        cerraElTuje += Time.deltaTime;
        if (cerraElTuje >= 3.0)
        {
            Vector3 spawnOrigin = new Vector3(Random.Range(width * -1 / 2, width / 2), Random.Range(height / 3, height / 2), 0);
            Instantiate(preFag, spawnOrigin, Quaternion.identity);
            cerraElTuje -= 3;
        }
                    
	}
}
