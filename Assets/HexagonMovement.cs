﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexagonMovement : MonoBehaviour {
    Transform playerTransform;
    float speed = 4;

	// Use this for initialization
	void Start () {
        playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 directionToPlayer = (playerTransform.position - transform.position).normalized * speed * Time.deltaTime; 
        transform.Translate(directionToPlayer);
	}
}
